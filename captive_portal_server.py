#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# camipot captive portal

import os
import sys
import signal
import logging
from argparse import ArgumentParser
from configparser import SafeConfigParser

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop, PeriodicCallback

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from redis import StrictRedis

from captive_portal.app import CaptivePortalApp
from captive_portal.billing import Billing

from captive_portal.models.base import Base

from lib.ratelimit import RateLimit

PREFIX = os.path.dirname(os.path.realpath(__file__))

DEFAULT_CFG_FILE = os.path.join(PREFIX, 'etc/captive_portal.ini')
CFG_SECTIONS = ['HTTP', 'DATABASE', 'BILLING', 'REDIS', 'LOGGING']
CRIT_CFG_VALS = {
    'HTTP': ['host', 'port'],
    'DATABASE': ['host', 'dbname', 'user', 'password'],
    'REDIS': ['host', 'port', 'password'],
    'PERIODIC': ['billing_sync', 'hotspots_reload'],
}


def stop_ioloop(signum, frame):
    logging.info('stop HTTP server')
    IOLoop.current().stop()


if __name__ == '__main__':
    try:
        # signals
        signal.signal(signal.SIGINT, stop_ioloop)
        signal.signal(signal.SIGTERM, stop_ioloop)

        # parse command line arguments
        parser = ArgumentParser(description="camipot server")
        parser.add_argument("-c", "--cfg_file", type=str,
                            action="store", help="a path to config file")
        parser.add_argument("-d", "--debug",
                            action="store_true", help="show debug info")
        parser.add_argument("-v", "--verbose",
                            action="store_true", help="show details")
        args = parser.parse_args()

        # initialize logging
        if args.debug:
            logging_level = logging.DEBUG
        else:
            logging_level = logging.INFO
        # logging formatter
        if args.verbose:
            logging_format = (u'%(filename)s[LINE:%(lineno)d]# ' +
                              u'%(levelname)s [%(asctime)s]  %(message)s')
        else:
            logging_format = \
                u'%(levelname)7s [%(asctime)s]  %(message)s'
        logging.basicConfig(level=logging_level, format=logging_format)

        # load config file
        cfg_file = args.cfg_file if args.cfg_file else DEFAULT_CFG_FILE
        cfg = SafeConfigParser()
        if not cfg.read(cfg_file):
            logging.error("couldn't load config ftom %s" % cfg_file)
            sys.exit(1)

        # check config
        for section in CRIT_CFG_VALS:
            if section not in cfg:
                logging.fatal("critical config section %s isn't defined" %
                              section)
                sys.exit(1)
            for opt in CRIT_CFG_VALS[section]:
                if opt not in cfg[section] or not cfg[section][opt]:
                    logging.fatal('incoorect config value [%s]:%s' %
                                  (section, opt))
                    sys.exit(1)

        # add logging file handler if a logging directory was specified
        # and access to wiriting is granted
        if 'LOGGING' in cfg and 'logdir'in cfg['LOGGING']:
            logdir = cfg['LOGGING']['logdir']
            if os.access(logdir, os.W_OK):
                log_file = os.path.join(logdir, 'captive_portal.log')
                # one day = one log file
                fh = logging.handlers.TimedRotatingFileHandler(
                                                        log_file,
                                                        when='midnight')
                fh.setFormatter(logging.Formatter(logging_format))
                root_logger = logging.getLogger()
                root_logger.addHandler(fh)
            else:
                logging.error("can't write logs to %s" % logdir)

        # connect to the database
        db_engine = create_engine('postgresql://%s:%s@%s/%s' % (
                                    cfg['DATABASE']['user'],
                                    cfg['DATABASE']['password'],
                                    cfg['DATABASE']['host'],
                                    cfg['DATABASE']['dbname']))
        Base.metadata.create_all(db_engine)
        # connect to redis
        redis_conn = StrictRedis(
                                host=cfg['REDIS']['host'],
                                port=cfg['REDIS']['port'],
                                password=cfg['REDIS']['password'],
                                decode_responses=True)

        # ioloop
        ioloop = IOLoop().current()
        # initialize billing connection
        try:
            billing = Billing(
                              host=cfg['BILLING']['host'],
                              dbname=cfg['BILLING']['dbname'],
                              user=cfg['BILLING']['user'],
                              local_db_engine=db_engine
            )
            billing.sync(db_engine)
            # periodic callback for syncing to the billing
            bill_per = PeriodicCallback(
                            lambda: billing.sync(db_engine),
                            1000*int(cfg['PERIODIC']['billing_sync']),
                            ioloop)
            bill_per.start()
        except:
            logging.error("couldn't sync to billing", exc_info=True)

        # rate limititation
        for pattern, val in cfg['RATE LIMIT'].items():
            try:
                cnt, sec = [int(x) for x in val.split(',')]
                RateLimit(pattern, cnt, sec, redis_conn)
                logging.debug('rate limit was set: %s' % pattern)
            except:
                logging.warning('an incorrect rate limit %s' % pattern,
                                exc_info=args.debug)

        # the app
        settings = {
            'debug': args.debug,
            'ios_workaround': cfg['UTILS']['ios_workaround']
        }
        app = CaptivePortalApp(db_engine, redis_conn, **settings)

        # periodic callback for reloading hotspots
        hotspots_reload_per = PeriodicCallback(
                        app.load_hotspots,
                        1000*int(cfg['PERIODIC']['hotspots_reload']),
                        ioloop)
        hotspots_reload_per.start()

        # HTTP server
        try:
            ssl_options = {
                'certfile': cfg['SSL']['certfile'],
                'keyfile': cfg['SSL']['keyfile'],
                'cert_reqs': 0,
            }
            logging.info('use SSL')
            httpserver = HTTPServer(app,
                                    ssl_options=ssl_options,
                                    xheaders=True)
        except:
            logging.info("don't use SSL", exc_info=args.debug)
            httpserver = HTTPServer(app, xheaders=True)
        httpserver.listen(cfg['HTTP']['port'], cfg['HTTP']['host'])
        logging.info('HTTP server started on %s:%s' % (
                                            cfg['HTTP']['host'],
                                            cfg['HTTP']['port']))
        ioloop.start()
    except Exception as err:
        logging.fatal('An unexpected error has occured', exc_info=True)
        exit(1)
