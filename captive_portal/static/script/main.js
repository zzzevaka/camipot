$("#phonefield").mask("+99999999999?999", {placeholder:" "});

$('.form-phone form').submit(function(){
	$.ajax({
		url: $(this).attr('action'),
		data: $(this).serialize(),
		complete: function (xhr, textStatus) {
			switch(xhr.status) {
				case 200:
					$(".form-phone").hide();
					break;
				case 500:
					show_err("Внутренняя ошибка сервера");
					break;
				case 417:
					show_err("Неверный формат номера телефона");
					break;
				case 403:
					show_err("Ошбика авторизации");
					break;
				default:
					show_err("Неизвестная ошибка");
			}
		}
	});
	return false;
});

$('.form-code form').submit(function(){
	$.ajax({
		url: $(this).attr('action'),
		data: $(this).serialize(),
		success: function(data) {
			window.location.replace(data);
		},
		complete: function(xhr, textStatus) {
			switch(xhr.status) {
				case 200:
					break;
				case 500:
					show_err("Внутренняя ошибка сервера");
					break;
				case 417:
					show_err("Неверный код. Перезагрузка страницы");
					setTimeout("window.location.reload()", 3000)
					break;
				default:
					show_err("Неизвестная ошибка");
			}
		}
	});
	return false;
});

function show_err(text) {
	$('#login-msg').finish();
	$('#login-msg').html(text);
	$('#login-msg').show();
	$('#login-msg').slideUp(3500);
}

$(document).ajaxSend(function() {
		original_submit_value = $('#login h1').html()
		$('#login h1').html('Подождите, идет загрузка')
        $('input[type="submit"]').attr('disabled', 'disabled')
}).ajaxComplete(function() {
		$('#login h1').html(original_submit_value)
        $('input[type="submit"]').removeAttr('disabled');
})


