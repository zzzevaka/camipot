#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# interface for a connection to RMT ASR

import os
import re
import time
import logging
import psycopg2
from .models.hotspot import SMSHotspot
from sqlalchemy.orm import sessionmaker


class Billing(object):
    '''
        RMT ASR synchronization
    '''

    def __init__(
                self, host, dbname, user, local_db_engine,
                password=None, port=None):
        self.host = host
        self.dbname = dbname
        self.user = user
        self.port = port
        self.password = password
        self.sessionmaker = sessionmaker(local_db_engine)

    def _get_bill_conn(self):
        return psycopg2.connect(
            host=self.host,
            dbname=self.dbname,
            user=self.user,
            port=self.port,
            password=self.password
        )

    def _get_asr_services(self):
        '''
            get array of services from asr
        '''
        services = []
        select = '''
            SELECT serv_id, serv.bill_id, soft_off, admin_off, ip,
                password, address, sms_url
            FROM serv JOIN serv2res USING (serv_id)
                JOIN resource_hotspot USING (res_id)
                JOIN ap USING (ap_id)
            WHERE %(now)i BETWEEN serv.start_date and serv.stop_date and
                %(now)i BETWEEN serv2res.start_date and serv2res.stop_date;
        ''' % {'now': time.time()}
        bill_conn = self._get_bill_conn()
        cursor = bill_conn.cursor()
        cursor.execute(select)
        for row in cursor:
            services.append({
                'serv_id': row[0],
                'bill_id': row[1],
                'on_flag': not (row[2] or row[3]),
                'ip': row[4],
                'password': row[5],
                'address': row[6],
                'sms_url': row[7]
            })
        cursor.close()
        bill_conn.close()
        if not services:
            raise BillingError('A responce from ASR is empty. \
                                    Request: %s' % select)
        return services

    def sync(self, sessionmaker):
        '''do synchroniztion'''
        session = self.sessionmaker()
        is_success = 1
        something_changed = 0
        checked = []
        # get services from ASR
        asr_serives = self._get_asr_services()
        hotspots = session.query(SMSHotspot).all()
        # for the each service
        for serv in asr_serives:
            # try to load a hotspot
            try:
                # find hotspot with this serv_id
                cur_hotspot = None
                for i in range(0, len(hotspots)):
                    if hotspots[i].id == serv['serv_id']:
                        cur_hotspot = hotspots.pop(i)
                        break
                # if hotspot doesn't exist create new
                if not cur_hotspot:
                    logging.debug(
                                'Hotspot (serv_id=%i ip=%s) not \
                                found. Create new'
                                % (int(serv['serv_id']), serv['ip']))
                    session.add(SMSHotspot(
                                    id=serv['serv_id'],
                                    ip=serv['ip'],
                                    acc_id=serv['bill_id'],
                                    password=serv['password'],
                                    sms_api_url=serv['sms_url']
                                ))
                    logging.debug('hotspot (serv_id=%i ip=%s)\
                        created' % (int(serv['serv_id']), serv['ip']))
                    something_changed = 1
                # hotspot has changed
                elif (
                        serv['ip'] != cur_hotspot.ip or
                        serv['on_flag'] != cur_hotspot.on_flag or
                        serv['password'] != cur_hotspot.password or
                        serv['sms_url'] != cur_hotspot.sms_api_url):
                    # update
                    cur_hotspot.ip = serv['ip']
                    cur_hotspot.on_flag = serv['on_flag']
                    cur_hotspot.password = serv['password']
                    cur_hotspot.sms_api_url = serv['sms_url']
                    logging.debug(
                                "hotspot %i (%s) changed" %
                                (serv['serv_id'], serv['ip']))
                    session.commit()
                    something_changed = 1
                # hotspot hasn't changed
                else:
                    logging.debug(
                            "hotspot %i (%s) didn't change" %
                            (serv['serv_id'], serv['ip']))
                checked.append(serv['serv_id'])
            except:
                logging.error(
                        "Hotspot syncing error (serv_id=%i ip=%s)" %
                        (int(serv['serv_id']), serv['ip']), exc_info=True)
                is_success = 0
                continue
        # stop hotspots which wasn't found in billing
        for hotspot in hotspots:
            if not hotspot.is_current():
                continue
            if hotspot.id in checked:
                continue
            else:
                hotspot.stop()
        # commit changes
        session.commit()
        session.close()
        ################################
        # BAD WAY OF RESTARTING RADIUS
        ################################
        if something_changed:
            cmd = '/usr/local/etc/rc.d/radiusd restart'
            radius_restart_result = os.system(cmd)
            if radius_restart_result:
                logging.error(
                        '%s returned %i' % (cmd, radius_restart_result))
                is_success = 0
            else:
                logging.info('radiusd succesfully restarted')
        return is_success, something_changed
