#!/usr/bin/env python
# -*- coding: utf-8 -*-

import io
import re
import logging
import os
import random
from functools import wraps

import tornado.web
import tornado.locale
import tornado.gen
import tornado.locale

from lib import smsgate, ppCaptcha
from lib.session import SessionHandler
from lib.ratelimit import RateLimitReached

from .models.hotspot import SMSHotspot
from .models.html_path import HTML_Path

DEFAULT_COUNTRY_PATTERN = '+7 (ddd) ddd-dd-dd'
CHECK_CODE_ATTEMPTS = 3
LOGIN_TTL = 15724800 # 182 days
PWD_SYM = 'abcdefghjkmnpqrstuvwxy'
PWD_SYM += PWD_SYM.upper()
PWD_SYM += '123456789'
ERROR_MESSAGES = dict(
    BLOCKED={'type': 'err_msg', 'msg': 'The request is blocked'},
    INVALID_PHONE={'type': 'ph_err', 'msg': 'Invalid phone number'},
    INVALID_CAPTCHA={'type': 'captcha_err', 'msg': 'Invalid symbols'},
    SMS_FAILED={'type': 'err_msg', 'msg': 'Couldn\'t send the SMS message'},
    UNKNOWN={'type': 'err_msg', 'msg': 'Internal server error'},
    INVALID_CODE_WITH_ATTEMPTS_COUNTER={
        'type': 'err_msg',
        'msg': 'Invalid code. Try again'
    }
)


class BaseHandler(SessionHandler):

    def initialize(self):
        '''
            and sets user locale
        '''
        self.hotspot = None
        if self.request.remote_ip in self.application.hotspots:
            self.hotspot = self.application.hotspots[self.request.remote_ip]
            logging.debug('got request from %s' % self.hotspot.ip)

    def get_user_locale(self):
        '''
            get user locale from:
                - arguments (prefer)
                - session
        '''
        locale = self.get_argument('locale', None)
        if locale in ('en_US', 'ru'):
            self.session['locale'] = locale
            return tornado.locale.get(locale)
        elif self.session['locale']:
            return tornado.locale.get(self.session['locale'])
        else:
            return None

    def get_current_user(self):
        '''check user permiission by ip'''
        return self.hotspot is not None

    def render_string(self, template_name, prefix=None, **kwargs):
        '''
            reload builit function

            loads a template from a directory specified for self.hotspot
            or from  the default directory
        '''
        try:
            prefix = prefix or self.hotspot.html_dir
            path = os.path.join(prefix, template_name)
            return super(BaseHandler, self).render_string(path, **kwargs)
        except FileNotFoundError:
            path = os.path.join(HTML_Path.get_default().get_path(),
                                template_name)
            return super(BaseHandler, self).render_string(path, **kwargs)

    def reset(self):
        '''path.split(os.sep
            flush the cookies, close the session and redirect user to
            an external page
        '''
        self.clear_all_cookies()
        self.session.finish()
        self.redirect('http://ya.ru')

    @property
    def cache(self):
        '''alias for application.cache connection'''
        return self.application.cache

    # user managment

    def save_user(self, mac, phone):
        '''
            save user in cache.
            Format: key=old:<cache_name>:<mac-address>. value=<phone>
            Example: old:public:F8F1B6EBE267 = '+71231234567'

            TEMP: hardcoded: remember user for a week (604800 sec)
        '''
        return self.cache.set(
            'old:%s:%s' % (self.hotspot.usercache_name, mac), phone, 604800
        )

    def forget_user(self, mac):
        '''delete user from cache'''
        return self.cache.delete(
            'old:%s:%s' % (self.hotspot.usercache_name, mac)
        )

    def is_old_user(self, mac):
        '''attempt to find user in cache by mac-address'''
        return self.cache.get('old:%s:%s' % (self.hotspot.usercache_name, mac))

    def add_to_radius(self, login, password):
        key = 'auth:%s:%s' % (self.hotspot.ip, login)
        self.cache.hset(key, 'pwd', password)
        self.cache.hset(
            key, 'session_timeout', self.hotspot.template.session_timeout
        )
        self.cache.hset(
            key, 'down_limit', self.hotspot.template.down_limit * 1024
        )
        self.cache.hset(
            key, 'rate-limit',
            '%ik/%ik' % (
                self.hotspot.template.up_rate,
                self.hotspot.template.down_rate
            )
        )
        self.cache.expire(key, LOGIN_TTL)

    def check_captcha(self):
        '''compares captcha from argumetns with session saved'''
        captcha_answer = self.get_argument('captcha', None)
        return captcha_answer.lower() == self.session['captcha'].lower()

    @staticmethod
    def check_captcha_decorator(redirect_to='/'):
        '''
            decorator

            if self.chec_captcha returns true: return decotated function result
            else: redirect to redirect_to (argument of decorator)

            example:

                @check_captcha_decorator(redirect_to='/error')
                def some_func(self):
                    pass
        '''
        def decorator(f):
            @wraps(f)
            def wrapper(self, *args, **kwargs):
                if self.check_captcha():
                    return f(self, *args, **kwargs)
                else:
                    self.session['err_msg'] = ERROR_MESSAGES['INVALID_CAPTCHA']
                    self.redirect(redirect_to)
            return wrapper
        return decorator

    def is_ios_cna(self):
        '''
            IOS Captive Network Assistant workaround
            When Apple device connects to WiFi network it makes
            a request to some URL and expects a certain HTTP answer.
            This request is sent with user-agent
            like '/CaptiveNetworkSupport.?/'.
        '''
        user_agent = self.request.headers.get('User-Agent')
        logging.debug('User-Agent: %s' % user_agent)
        if not user_agent:
            return False
        return 'CaptiveNetworkSupport' in user_agent

    @staticmethod
    def ios_cna_workaround(f):
        '''
            decorator

            If the server catches a request from IOS CNA
            it falsifies the answer and an IOS device thinks
            what the Internet is opened
        '''
        def wrapper(self, *args, **kwargs):
            if int(self.application.settings.get('ios_workaround')):
                if self.is_ios_cna():
                    self.finish(
                            '<HTML><HEAD><TITLE>Success' +
                            '</TITLE></HEAD><BODY>Success</BODY></HTML> ')
                    return
            return f(self, *args, **kwargs)
        return wrapper


class MainHandler(BaseHandler):
    '''
        handler for /

        expects HTTP arguments:
            - reset: if defined the session will be flushed
            - mac: MAC address of user's device
            - link-login-only: URI of router login page
            - ling-orig: URI which user tried to liad
    '''

    @tornado.web.authenticated
    @BaseHandler.ios_cna_workaround
    def post(self):
        try:
            # HTML template arguments
            country_pattern = self.session['country_pattern']
            kwargs = {
                'session_id': self.session.sid,
                'country_pattern': country_pattern or DEFAULT_COUNTRY_PATTERN
            }
            # reset session
            if self.get_argument('reset', 0):
                self.reset()
                return
            # device must be forgotten
            if self.get_argument('ignore_known', 0):
                self.forget_user(self.session['mac'])
                self.reset()
                return
            # show a blocking page request if the hotspot is blocked
            if not self.hotspot.on_flag:
                self.session['err_msg'] = ERROR_MESSAGES['BLOCKED']
                this.redirect('/error')
                return
            # get arguments
            self.session['mac'] = self.get_argument(
                'mac',
                self.session['mac']
            )
            self.session['link-login-only'] = self.get_argument(
                'link-login-only',
                self.session['link-login-only']
            )
            self.session['link-orig'] = self.get_argument(
                'link-orig',
                self.session['link-orig']
            )
            # check arguments
            for val in ('mac', 'link-login-only', 'link-orig'):
                if not self.session[val]:
                    raise tornado.web.MissingArgumentError(val)
            # get phone number if MAC-address in cache
            old_user_phone = self.is_old_user(self.session['mac'])
            if old_user_phone:
                logging.info(
                    'Known users: %s: %s' % (
                        self.session['mac'], old_user_phone
                    )
                )
                # show page for known user
                self.session['allow_access'] = 1
                self.session['phone'] = old_user_phone
                # render html
                kwargs['phone'] = old_user_phone
                form = self.render_string('known.html', **kwargs)
                self.render('index.html', form=form)
                return
            else:
                logging.info("it isn't an old device %s" % self.session['mac'])
            if self.session['step']:
                if self.session['step'] == 'send_sms':
                    self.redirect('/check_code')
                    return
            # it's an unknown device
            self.session['force_auth'] = 0
            self.session['step'] = 'main_handler'
            # send normal page
            if self.session['err_msg']:
                kwargs[self.session['err_msg']['type']] =\
                    self.session['err_msg']['msg']
                self.session['err_msg'] = ''
            if self.session['phone']:
                kwargs['ph_num'] = self.session['phone']
            form = self.render_string('getphone.html', **kwargs)
            self.render('index.html', form=form)
        except tornado.web.MissingArgumentError:
            self.redirect('http://yandex.ru')
        except tornado.web.Finish:
            pass
        except:
            logging.error('An error has occured:', exc_info=True)
            self.session['err_msg'] = ERROR_MESSAGES['UNKNOWN']
            self.redirect('/error')

    def get(self):
        return self.post()


class GetCodeHandler(BaseHandler):
    '''
        handler for /getcode

        expects HTTP arguments:
            - session_id: no comments
            - phone: phone in format +7(123)1234567
            - captcha: captcha value

        If user has not passed '/' the session will be flushed
    '''

    @tornado.web.authenticated
    @tornado.gen.coroutine
    def post(self):
        try:
            kwargs = {'session_id': self.session.sid}
            if self.session['step'] != 'main_handler':
                self.reset()
                return
            # remember user country
            if self.get_argument('country_pattern', None):
                self.session['country_pattern'] =\
                    self.get_argument('country_pattern')
            # delete extra symbols from the phone number
            phone = re.sub('[^\d+]', '', self.get_argument('phone'))
            sms = smsgate.SMSGate(api_url=self.hotspot.sms_api_url)
            # if phone format is valid save phone to session
            if sms.is_valid_number(phone):
                self.session['phone'] = phone
            # or show error
            else:
                self.session['phone'] = ''
                self.session['err_msg'] = ERROR_MESSAGES['INVALID_PHONE']
                self.redirect('/')
                return
            if not self.check_captcha():
                self.session['err_msg'] = ERROR_MESSAGES['INVALID_CAPTCHA']
                self.redirect('/')
                return
            # create code for SMS
            code = str(random.randint(1111, 9999))
            # sms sending
            sms_text = self.locale.translate(self.hotspot.sms_text) %\
                {'code': code}
            # send SMS
            is_sms_sended = yield sms.send(
                phone,
                sms_text,
                ratelimit=[
                    'sms_hour_all',
                    'sms_day_all',
                    'sms_hour_hotspot_%s' % self.hotspot.ip,
                    'sms_day_hotspot_%s' % self.hotspot.ip,
                    'sms_minute_mac_%s' % self.session['mac'],
                    'sms_hour_mac_%s' % self.session['mac'],
                    'sms_day_mac_%s' % self.session['mac']
                ]
            )
            if is_sms_sended:
                self.session['step'] = 'send_sms'
                self.session['check_code_attempts'] = CHECK_CODE_ATTEMPTS
                self.session['phone'] = phone
                self.session['code'] = code
                # send page for the code checking
                form = self.render_string('getcode.html', **kwargs)
                self.render('index.html', form=form, **kwargs)
            else:
                self.session['err_msg'] = ERROR_MESSAGES['SMS_FAILED']
                self.redirect('/error')
        except RateLimitReached as err:
            logging.debug('ratelimit reached by marker %s' % err)
            self.session['err_msg'] = ERROR_MESSAGES['BLOCKED']
            self.redirect('/error')
        except:
            logging.error('An error has occured:', exc_info=True)
            self.session['err_msg'] = ERROR_MESSAGES['UNKNOWN']
            self.redirect('/error')


class CheckCodeHandler(BaseHandler):
    '''
        handler form /check_code

        expects HTTP arguments:
            - code: code from SMS
    '''

    @tornado.web.authenticated
    def post(self):
        kwargs = {'session_id': self.session.sid}
        #
        # ?? REPLACE STEP CHECKING BY DEVOCRATOR ???
        #
        if self.session['step'] != 'send_sms':
            self.reset()
            return
        if self.get_argument('code', None):
            # compare the code from session info
            # and from request arguments
            if self.session['code'] == self.get_argument('code'):
                self.session['allow_access'] = 1
                self.redirect('/confirm')
                return
            else:
                self.session['check_code_attempts'] -= 1
            # if the values aren't equal
            if self.session['check_code_attempts']:
                # send a page with an error
                self.session['err_msg'] =\
                    ERROR_MESSAGES['INVALID_CODE_WITH_ATTEMPTS_COUNTER']
            # attempts is over
            else:
                self.reset()
                return
        if self.session['err_msg']:
                kwargs[self.session['err_msg']['type']] =\
                    self.session['err_msg']['msg']
                self.session['err_msg'] = ''
        form = self.render_string('getcode.html', **kwargs)
        self.render('index.html', form=form)

    def get(self):
        return self.post()


class ConfirmHandler(BaseHandler):
    '''
        handler for /confirm

        if self.session['allow_access'] is True: access will be opened
        else: reset session
    '''

    @tornado.web.authenticated
    def get(self):
        try:
            # reset session if allow_set wasn't defined
            if not self.session['allow_access']:
                self.reset()
                return
            # prepare login
            login = '%s@%s' % (
                re.sub('[^\w]', '', self.session['phone']),
                self.session['mac']
            )
            otpwd = ''.join(random.choice(PWD_SYM) for x in range(8))
            self.add_to_radius(login, otpwd)
            self.clear_cookie('session_id')
            # change https to http
            # MikroTik Login page available by HTTP and HTTPS both.
            # There are troubles can be with HTTPS, so use HTTP.
            link_login = re.sub('https', 'http',
                                self.session['link-login-only'])
            response_url = self.hotspot.loging_url % {
                                'link-orig': self.session['link-orig'],
                                'link-login': link_login,
                                'login': login,
                                'pwd': otpwd
                            }
            self.redirect(response_url)
            self.save_user(self.session['mac'], self.session['phone'])
            return
        except:
            logging.error('An error has occured:', exc_info=True)
            self.session['err_msg'] = ERROR_MESSAGES['UNKNOWN']
            self.redirect('/error')


class ErrorHandler(BaseHandler):
    '''
        render error.html with self.session['err_msg']
    '''

    @tornado.web.authenticated
    def get(self):
        kwargs = {'session_id': self.session.sid}
        if self.session['err_msg']:
            kwargs[self.session['err_msg']['type']] =\
                self.session['err_msg']['msg']
            self.session['err_msg'] = ''
            form = self.render_string('error.html', **kwargs)
            self.render('index.html', form=form)


class DescribeHandler(BaseHandler):
    '''
        just returns describe.html
    '''

    @tornado.web.authenticated
    def get(self):
        form = self.render_string('describe.html')
        self.render('index.html', form=form)


class CaptchaHandler(BaseHandler):
    '''
        returns captcha image and writes a capcta code to the session
    '''

    def get(self):
        code_img = ppCaptcha.CreatePPCaptcha(
            chars=''.join(str(x) for x in range(0, 10)),
            size=(230, 40),
            length=5,
            draw_lines=False
        )
        self.session["captcha"] = code_img[1]
        o = io.BytesIO()
        code_img[0].save(o, "JPEG")
        s = o.getvalue()
        self.set_header('Content-type', 'image/jpg')
        self.finish(s)


class ExampleHandler(BaseHandler):
    '''
        just returns default/example.html
    '''

    def get(self):
        kwargs = {
            'country_pattern': DEFAULT_COUNTRY_PATTERN
        }
        t_dir = self.get_argument('template', None)
        if t_dir:
            self.session['t_dir'] = t_dir
        else:
            t_dir = self.session['t_dir'] or 'default'
        form = self.render_string('example.html', prefix=t_dir, **kwargs)
        self.render('index.html', prefix=t_dir, form=form)


class LoginHandler(tornado.web.RequestHandler):

    def get(self):
        raise tornado.web.HTTPError(403)
