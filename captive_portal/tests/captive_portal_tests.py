#!/usr/bin/env python3

import __main__
import unittest
import logging

from tornado.testing import AsyncHTTPTestCase
from tornado.httpclient import AsyncHTTPClient, HTTPRequest

from captive_portal import app, handlers
from captive_portal.models import *


class CaptivePortal1ModelsTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.s = __main__.session_maker()

    def test1_HTML_Path_create(self):
        p = html_path.HTML_Path(acc_id=__main__.ACC_ID,
                                name='testpath')
        self.s.add(p)
        self.s.commit()

    def test2_HTML_Path_check(self):
        p = self.s.query(html_path.HTML_Path).filter_by(
                                            name='testpath').first()
        self.assertEqual(p.get_path(), 'testpath')

    def test3_HTML_Path_default(self):
        p = html_path.HTML_Path.get_default()
        self.assertEqual(p.name, 'default')

    def test4_Template_default(self):
        t = template.Template.get_default()
        self.assertEqual(t.name, 'default')

    def test5_Template_create(self):
        t = template.Template(
                    name='test_template',
                    down_rate=1024,
                    up_rate=512,
                    session_timeout=60)
        self.s.add(t)
        self.s.commit()

    def test6_Template_set_attr(self):
        with self.assertRaises(ValueError):
            t = self.s.query(template.Template).filter_by(
                                        name='test_template').first()
            t.down_rate = 'not int'

    def test71_SMSHotspot_create(self):
        h = hotspot.SMSHotspot(
                    ip='127.0.0.1',
                    password='qwerty',
                    sms_api_url='localhost')
        self.s.add(h)
        self.s.commit()

    def test72_SMSHotspot_name(self):
        h = self.s.query(hotspot.SMSHotspot).filter_by(ip='127.0.0.1').first()
        self.assertEqual(h.name, '127.0.0.1')
        h.name = 'test_hotspot'
        self.assertEqual(h.name, 'test_hotspot')

    def test73_SMSHotspot_set_name(self):
        h = self.s.query(hotspot.SMSHotspot).filter_by(ip='127.0.0.1').first()
        with self.assertRaises(ValueError):
            h.name = "Very very very long name"

    def test74_SMSHotspot_html_dir(self):
        h = self.s.query(hotspot.SMSHotspot).filter_by(ip='127.0.0.1').first()
        self.assertEqual(h.html_dir, 'testpath')

    def test81_Template_how_many_use(self):
        t = self.s.query(template.Template).filter_by(
                                            name='test_template').first()
        self.assertEqual(t.how_many_use(), 1)


class CaptivePortal2HandlersTestCase(AsyncHTTPTestCase):

    def get_app(self):
        setting = {'ios_workaround': True}
        return app.CaptivePortalApp(
            __main__.db_engine,
            __main__.redis_conn,
            **setting
        )

    def test1_main(self):
        r = self.fetch(
                '/?mac=F8:F1:B6:EB:E2:67&link-orig=http://lenta.ru/&' +
                'link-login-only=https://rumyantsevo.hs.credo-telecom.ru/' +
                'login')
        self.assertEqual(r.code, 200)

    def test2_example(self):
        r = self.fetch('/example')
        self.assertEqual(r.code, 200)

    def test3_ios_workaround(self):
        r = self.fetch(
                '/?mac=F8:F1:B6:EB:E2:67&link-orig=http://lenta.ru/&' +
                'link-login-only=https://rumyantsevo.hs.credo-telecom.ru/' +
                'login',
                user_agent='blabla_CaptiveNetworkSupport_blablabla',
        )
        self.assertEqual(r.body,
                         b'<HTML><HEAD><TITLE>Success</TITLE></HEAD>' +
                         b'<BODY>Success</BODY></HTML> ')
