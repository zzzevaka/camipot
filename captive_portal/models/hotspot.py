#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ipaddress
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, and_
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_method

from .base import AbstactStartStopModel
from .template import Template
from .html_path import HTML_Path, DEFAULT_HTML_PATH_ID


class AbstractHotspot(AbstactStartStopModel):

    MAX_NAME_LEN = 20
    DEFAULT_USERCAHE_NAME = 'public'

    __abstract__ = True

    id = Column(Integer, primary_key=True)
    acc_id = Column(Integer, nullable=False, index=True)
    ip = Column(String(50), nullable=False, index=True)
    __name = Column('name', String(MAX_NAME_LEN))
    password = Column(String, nullable=False)
    address = Column(String(), default='unknown')
    on_flag = Column(Integer(), default=1, index=True)
    usercache_name = Column(String(30), default=DEFAULT_USERCAHE_NAME)

    # relationships

    @declared_attr
    def html_path_id(cls):
        return Column(Integer, ForeignKey('html_path.id'))

    @declared_attr
    def html_path(cls):
        return relationship('HTML_Path', backref='hotspots', lazy='joined')

    @declared_attr
    def template_id(cls):
        return Column(Integer, ForeignKey('template.id'))

    @declared_attr
    def template(cls):
        return relationship('Template', backref='hotspots', lazy='joined')

    def __init__(
                    self, ip, password,
                    template_id=Template.DEFAULT_TEMPLATE_ID,
                    usercache_name=DEFAULT_USERCAHE_NAME,
                    acc_id=1, id=None,
                    name='', address='unknown', on_flag=1,
                    html_path_id=DEFAULT_HTML_PATH_ID,
                    *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.ip = ip
        self.id = id
        self.password = password
        self.template_id = template_id
        self.html_path_id = html_path_id
        self.name = name
        self.acc_id = acc_id
        self.__name = name
        self.address = address
        self.on_flag = on_flag
        self.usercache_name = usercache_name

    def __setattr__(self, attr, value):
        # check types
        if attr in ('password', 'name', 'address'):
            value = str(value)
        elif attr in ('template_id', 'usercache_id', 'acc_id', 'on_flag'):
            value = int(value)
        elif attr in ('ip',):
            value = str(ipaddress.ip_address(value))
        super(AbstractHotspot, self).__setattr__(attr, value)

    @property
    def name(self):
        return self.__name or self.ip

    @name.setter
    def name(self, value):
        if len(value) > self.MAX_NAME_LEN:
            raise ValueError('too long name (max length: %i)' %
                             self.MAX_NAME_LEN)
        self.__name = value

    @property
    def loging_url(self):
        return '%(link-login)s?dst=%(link-orig)s&' + \
                'username=%(login)s&password=%(pwd)s'

    @property
    def html_dir(self):
        return self.html_path.get_path()


class SMSHotspot(AbstractHotspot):

    __tablename__ = 'hotspot'

    sms_api_url = Column(String(150), nullable=False)
    sms_text = Column(String(), nullable=False)

    def __init__(
            self, sms_api_url,
            sms_text='Your code: %(code)s',
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sms_api_url = sms_api_url
        self.sms_text = sms_text

    def __setattr__(self, attr, value):
        if attr in ('sms_api_url',):
            value = str(value)
        super(SMSHotspot, self).__setattr__(attr, value)
