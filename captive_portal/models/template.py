#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, DateTime, and_
from .base import AbstactStartStopModel


class Template(AbstactStartStopModel):

    DEFAULT_ID = 1
    DEFAULT_NAME = 'default'
    DEFAULT_ACC_ID = 1
    DEFAULT_TEMPLATE_ID = 1
    DEFAULT_HTML_PATH_ID = 1

    __tablename__ = 'template'

    id = Column(Integer, primary_key=True)
    acc_id = Column(Integer, nullable=False, index=True)
    name = Column(String(50), default='template')
    down_rate = Column(Integer, default=0)
    up_rate = Column(Integer, default=0)
    down_limit = Column(Integer, default=0)
    session_timeout = Column(Integer, default=0)

    def __init__(
                self, acc_id=DEFAULT_ACC_ID, id=None, name='',
                down_rate=0, up_rate=0, down_limit=0,
                session_timeout=0, *args, **kwargs):

        super().__init__(*args, **kwargs)
        self.id = id
        self.acc_id = acc_id
        self.name = name
        self.down_rate = down_rate
        self.up_rate = up_rate
        self.down_limit = down_limit
        self.session_timeout = session_timeout

    def __setattr__(self, attr, value):
        # check types
        if attr in ('name', 'html'):
            value = str(value)
        elif attr in ('acc_id', 'down_rate', 'up_rate', 'dwon_limit',
                      'session_timeout'):
            value = int(value)
        super(Template, self).__setattr__(attr, value)

    def how_many_use(self):
        return len(self.hotspots)

    def __sortkey__(self):
        return self.id

    @classmethod
    def get_default(cls):
        return cls(
                    id=cls.DEFAULT_ID,
                    acc_id=cls.DEFAULT_ACC_ID,
                    name=cls.DEFAULT_NAME,
                    down_rate=0,
                    up_rate=0,
                    down_limit=0,
                    session_timeout=0)
