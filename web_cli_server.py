#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import signal
import logging
from argparse import ArgumentParser
from configparser import SafeConfigParser

from redis import StrictRedis
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.httpserver import HTTPServer

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from web_cli.app import WebCliApp

PREFIX = os.path.dirname(os.path.realpath(__file__))

DEFAULT_CFG_FILE = os.path.join(PREFIX, 'etc/web_cli.ini')
CRIT_CFG_VALS = {
    'HTTP': ['host', 'port'],
    'DATABASE': ['host', 'dbname', 'user', 'password'],
    'REDIS': ['host', 'port', 'password'],
}


def stop_ioloop(signum, frame):
    logging.info('stop HTTP server')
    IOLoop.current().stop()

if __name__ == '__main__':
    try:
        # signals
        signal.signal(signal.SIGINT, stop_ioloop)
        signal.signal(signal.SIGTERM, stop_ioloop)

        # parse command line arguments
        parser = ArgumentParser(description="camipot web interface")
        parser.add_argument("-c", "--cfg_file", type=str,
                            action="store", help="a path to config file")
        parser.add_argument("-d", "--debug",
                            action="store_true", help="show debug info")
        parser.add_argument("-v", "--verbose",
                            action="store_true", help="show details")
        args = parser.parse_args()

        # initialize logging
        if args.debug:
            logging_level = logging.DEBUG
        else:
            logging_level = logging.INFO
        # logging formatter
        if args.verbose:
            logging_format = '%(filename)s[LINE:%(lineno)d]# ' + \
                             '%(levelname)s [%(asctime)s]  %(message)s'
        else:
            logging_format = '%(levelname)s [%(asctime)s]  %(message)s'
        logging.basicConfig(level=logging_level, format=logging_format)

        # load config file
        cfg_file = args.cfg_file if args.cfg_file else DEFAULT_CFG_FILE
        cfg = SafeConfigParser()
        if not cfg.read(cfg_file):
            logging.error("couldn't load config ftom %s" % cfg_file)
            sys.exit(1)

        # check config
        for section in CRIT_CFG_VALS:
            if section not in cfg:
                logging.fatal("critical config section %s isn't defined" %
                              section)
                sys.exit(1)
            for opt in CRIT_CFG_VALS[section]:
                if opt not in cfg[section] or not cfg[section][opt]:
                    logging.fatal('incoorect config value [%s]:%s' %
                                  (section, opt))
                    sys.exit(1)

        # add logging file handler if a logging directory was specified
        # and access to wiriting is granted
        if 'LOGGING' in cfg and 'logdir'in cfg['LOGGING']:
            logdir = cfg['LOGGING']['logdir']
            if os.access(logdir, os.W_OK):
                log_file = os.path.join(logdir, 'web_cli.log')
                # one day = one log file
                fh = logging.handlers.TimedRotatingFileHandler(
                                                log_file, when='midnight')
                fh.setFormatter(logging.Formatter(logging_format))
                root_logger = logging.getLogger()
                root_logger.addHandler(fh)
            else:
                logging.error("can't write logs to %s" % logdir)

        # connect to the database
        db_engine = create_engine('postgresql://%s:%s@%s/%s' % (
                                    cfg['DATABASE']['user'],
                                    cfg['DATABASE']['password'],
                                    cfg['DATABASE']['host'],
                                    cfg['DATABASE']['dbname']))

        # connect to redis
        redis_conn = StrictRedis(
                        host=cfg['REDIS']['host'],
                        port=cfg['REDIS']['port'],
                        password=cfg['REDIS']['password'],
                        decode_responses=True)

        # ioloop
        ioloop = IOLoop().current()

        # the app
        settings = {'debug': args.debug}
        app = WebCliApp(db_engine, redis_conn, **settings)

        # HTTP server
        try:
            ssl_options = {
                'certfile': cfg['SSL']['certfile'],
                'keyfile': cfg['SSL']['keyfile'],
                'cert_reqs': 0,
            }
            httpserver = HTTPServer(app, ssl_options=ssl_options,
                                    xheaders=True)
            logging.info('use SSL')
        except:
            logging.debug('SSL off cause', exc_info=True)
            logging.info("don't use SSL")
            httpserver = HTTPServer(app, xheaders=True)
        httpserver.listen(cfg['HTTP']['port'], cfg['HTTP']['host'])
        logging.info('start HTTP server on %s:%s' % (
                                            cfg['HTTP']['host'],
                                            cfg['HTTP']['port']))
        ioloop.start()
    except Exception as err:
        logging.fatal('An unexpected error has occured', exc_info=True)
        exit(1)
