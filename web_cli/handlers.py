#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from time import time
import logging
from ipaddress import ip_address, ip_network

import tornado.web
from sqlalchemy import and_, or_

from lib.session import SessionHandler
from captive_portal.models.template import Template
from captive_portal.models.hotspot import SMSHotspot
from captive_portal.models.html_path import HTML_Path

from lib.dataTable import TornadoDataTable

ASR_NETWORK = ip_network('158.250.255.0/24')
CLI_SESSION_TIMEOUT = 3600*12
CLI_SESSION_LONG_TIMEOUT = 3600*24*90
DEFAULT_ACC_ID = 1


class BaseCliHandler(SessionHandler):

    def initialize(self):
        self.db = self.application.sessionmaker()

    def on_finish(self):
        self.db.close()
        super(BaseCliHandler, self).on_finish()

    def get_current_user(self):
        return self.session['acc_id']


class LoginHandler(BaseCliHandler):

    def get(self):
        self.redirect('http://ya.ru')


class GetSessionHandler(SessionHandler):

    def get_current_user(self):
        '''
            just returns session_id

            request from not trusted hosts will be rejected
            look at ASR_NETWORK

            WARNING: SessioHandler hashes an session id by User-Agent.
            You have to fake user's User-Agent
        '''
        return ip_address(self.request.remote_ip) in ASR_NETWORK

    @tornado.web.authenticated
    def get(self):
        self.session['acc_id'] = self.get_argument('acc_id')
        self.session['username'] = 'maksimov'
        self.session.set_ttl(CLI_SESSION_TIMEOUT)
        self.finish(self.session.sid)


class StatHandler(BaseCliHandler):

    @tornado.web.authenticated
    def get(self):
        self.render('stat.html',
                    user=self.session['usernname'],
                    session_id=self.session.sid)


class ManageHandler(BaseCliHandler):

    @tornado.web.authenticated
    def get(self):
        now = time()
        acc_id = self.session['acc_id']
        username = self.session['username']
        templates = self.db.query(Template).filter(
                            or_(Template.acc_id == acc_id,
                                Template.acc_id == DEFAULT_ACC_ID),
                            Template.is_current()
                        ).order_by(Template.id).all()
        hotspots = self.db.query(SMSHotspot).filter(
                            SMSHotspot.is_current(),
                            SMSHotspot.acc_id == self.session['acc_id']
                        ).order_by(SMSHotspot.id).all()
        self.render('manage2.html',
                    user=self.session['usernname'],
                    templates=templates,
                    hotspots=hotspots,
                    session_id=self.session.sid)


class NewTemplateHandler(BaseCliHandler):

    @tornado.web.authenticated
    def post(self):
        template = None
        args = {}
        try:
            for val in ('name', 'down_rate', 'up_rate',
                        'down_limit', 'session_timeout'):
                args[val] = self.get_argument(val)
                if not args[val]:
                    errmsg = "Поле '%s' не может быть пустым" % val
                    self.render('error.html', errmsg=errmsg,
                                session_id=self.session.sid)
                    return
            try:
                template = Template(
                                name=args['name'],
                                acc_id=self.session['acc_id'],
                                down_rate=args['down_rate'],
                                up_rate=args['up_rate'],
                                down_limit=args['down_limit'],
                                session_timeout=args['session_timeout'])
                self.db.add(template)
                self.db.commit()
                self.redirect('/manage')
                return
            except:
                logging.error("couldn't create a hotspot", exc_info=True)
                errmsg = "Неизвестная ошибка"
                self.render('error.html', errms=errmsg,
                            session_id=self.session.sid)
                return
        except tornado.web.MissingArgumentError:
            self.render('new_template.html',
                        session_id=self.session.sid)
        except:
            logging.error('An unexpected error has occured', exc_info=True)
            self.render('error.html', errms=errmsg,
                        session_id=self.session.sid)


class DelTemplateHandler(BaseCliHandler):

    @tornado.web.authenticated
    def post(self):
        try:
            id = self.get_argument('id')
            template = self.db.query(Template).filter(
                                Template.id == id,
                                Template.is_current(),
                                Template.acc_id == self.session['acc_id']
                            ).first()
            if not template:
                raise tornado.web.HTTPError(500)
            use_template_count = template.how_many_use()
            logging.error(use_template_count)
            if not use_template_count:
                template.stop()
                self.db.commit()
                self.redirect('/manage')
            else:
                errmsg = 'Этот шаблон нельзя удалить, т.к. он используется'
                self.render('error.html', errmsg=errmsg,
                            session_id=self.session.sid)
        except:
            logging.error('An unexpected error has occured', exc_info=True)
            errmsg = 'Неизвестная ошибка'
            self.render('error.html', errms=errmsg,
                        session_id=self.session.sid)


class EditTemplateHandler(BaseCliHandler):

    @tornado.web.authenticated
    def post(self):
        try:
            template = self.db.query(Template).filter(
                                Template.id == self.get_argument('id'),
                                Template.is_current(),
                                Template.acc_id == self.session['acc_id']
                            ).first()
            if not template:
                raise tornado.web.HTTPError(500)
            for attr in ('name', 'down_rate', 'up_rate', 'down_limit',
                         'session_timeout'):
                setattr(template,
                        attr,
                        self.get_argument(attr))
            self.db.commit()
            self.redirect('/manage')
        except tornado.web.MissingArgumentError:
            self.render('edit_template.html',
                        session_id=self.session.sid,
                        template=template)
        except:
            logging.error('An unexpected error has occured', exc_info=True)
            errmsg = 'Неизвестная ошибка'
            self.render('error.html', errms=errmsg,
                        session_id=self.session.sid)


class EditHotspotHandler(BaseCliHandler):

    @tornado.web.authenticated
    def post(self):
        try:
            logging.error(self.get_argument('id', 0))
            logging.error(self.session['acc_id'])
            hotspot = self.db.query(SMSHotspot).filter(
                                SMSHotspot.id == self.get_argument('id', 0),
                                SMSHotspot.is_current(),
                                SMSHotspot.acc_id == self.session['acc_id']
                            ).first()
            if not hotspot:
                raise tornado.web.HTTPError(500)
            templates = self.db.query(Template).filter(
                            or_(
                                Template.acc_id == self.session['acc_id'],
                                Template.acc_id == DEFAULT_ACC_ID),
                            Template.is_current()
                            ).all()
            html_paths = self.db.query(HTML_Path).filter(
                            or_(
                                HTML_Path.acc_id == self.session['acc_id'],
                                HTML_Path.acc_id == DEFAULT_ACC_ID
                            ),
                        ).all()
            for attr in ('name', 'template_id', 'html_path_id'):
                setattr(hotspot,
                        attr,
                        self.get_argument(attr))
            self.db.commit()
            self.redirect('/manage')
        except tornado.web.MissingArgumentError:
            self.render('edit_hotspot.html',
                        session_id=self.session.sid,
                        hotspot=hotspot,
                        templates=templates,
                        html_paths=html_paths)
        except:
            logging.error('An unexpected error has occured', exc_info=True)
            errmsg = 'Неизвестная ошибка'
            self.render('error.html', errmsg=errmsg,
                        session_id=self.session.sid)


class GetUserSessionInfo(BaseCliHandler):

    @tornado.web.authenticated
    def post(self):
        request = '''
            (SELECT
                DISTINCT ON (radacctid) radacctid, username,
                hotspot.name hotspot_name, callingstationid,
                to_char(acctstarttime, 'YYYY-MM-DD HH24:MI:SS') start_session,
                to_char(acctstoptime, 'YYYY-MM-DD HH24:MI:SS') stop_session,
                acctterminatecause
            FROM radacct JOIN hotspot
                ON radacct.nasipaddress = inet(hotspot.ip)
            WHERE hotspot.acc_id = %i
            ORDER BY radacctid DESC, stop_date DESC) t
        ''' % int(self.session['acc_id'])
        logging.debug('get session info of users')
        dataTable = TornadoDataTable(
            sql_conn=self.db.connection().connection,
            columns=[
                    'hotspot_name',
                    "substring(username from '(\d+)@')",
                    'callingstationid',
                    'start_session',
                    'stop_session'],
            from_block=request,
            arguments=self.request.arguments
        )
        json_data = dataTable.get_data()
        self.finish(json_data)
