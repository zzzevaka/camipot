#!/usr/bin/env python3
from os import path

import redis
import sys
from configparser import SafeConfigParser
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from lib.tests.ratelimit_test import *
from lib.tests.session_test import *
from lib.tests.ppCaptcha_test import *
from captive_portal.tests.captive_portal_tests import *
from captive_portal.models.base import Base

PREFIX = path.dirname((path.realpath(__file__)))
CONFIG_FILE = path.join(PREFIX, 'etc/test.cfg')
ACC_ID = 10


if __name__ == '__main__':

    cfg = SafeConfigParser()
    cfg.read(CONFIG_FILE)

    # sql database connection
    # connect to the database
    db_engine = create_engine(
                            'postgresql://%s:%s@%s/%s' % (
                                cfg['DATABASE']['user'],
                                cfg['DATABASE']['password'],
                                cfg['DATABASE']['host'],
                                cfg['DATABASE']['dbname']))
    session_maker = sessionmaker(db_engine)

    # redis connection
    redis_conn = redis.StrictRedis(
            host=cfg['REDIS']['host'],
            port=cfg['REDIS']['port'],
            password=cfg['REDIS']['password'])

    # do flushall un redis
    redis_conn.flushall()
    # delete all from test database
    with db_engine.begin() as conn:
        conn.execute('TRUNCATE html_path, template RESTART IDENTITY CASCADE')
    # Base.metadata.create_all(db_engine)

    unittest.main()
