#!/usr/bin/env python3

import unittest
import redis
import __main__
from ..ratelimit import RateLimit, RateLimitReached


@RateLimit.decorator
def dummy():
    pass


class RateLimitTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.redis_conn = __main__.redis_conn

    def test1_singlemarker(self):
        RateLimit('test_10s', 5, 10, self.redis_conn)
        with self.assertRaises(RateLimitReached):
            for x in range(0, 6):
                dummy(ratelimit='test_10s')

    def test_fewmarkers(self):
        RateLimit('test_2s', 15, 2, self.redis_conn)
        RateLimit('test_10s', 10, 10, self.redis_conn)
        with self.assertRaises(RateLimitReached) as cm:
            for x in range(0, 16):
                dummy(ratelimit=['test_2s', 'test_10s'])
        self.assertEqual(str(cm.exception), 'test_10s')
