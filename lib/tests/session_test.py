#!/usr/bin/env python3

import unittest
import redis
import __main__
from ..session import Session, SessionError


REDIS_HOST = 'localhost'
REDIS_PORT = 6380
REDIS_PASSWORD = 'hfpldfnhb'


class SessionTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.redis_conn = __main__.redis_conn

    def test_is_new(self):
        session = Session(
            redis_conn=self.redis_conn,
            ttl=30,
            hash_string='test1'
        )
        session['name'] = 'Leo'
        self.assertEqual(session.is_new(), 1)

    def test_save(self):
        session = Session(
            redis_conn=self.redis_conn,
            ttl=30,
            hash_string='test2'
        )
        session['name'] = 'Leo'
        session.save()
        session.unlock()
        session2 = Session(
            sid=session.sid,
            redis_conn=self.redis_conn,
            ttl=30,
            hash_string='test2'
        )
        self.assertEqual(session2['name'], 'Leo')

    def test_hash(self):
        session = Session(
            redis_conn=self.redis_conn,
            ttl=30,
            hash_string='test4'
        )
        session['name'] = 'Leo'
        session.save()
        session.unlock()
        session2 = Session(
            redis_conn=self.redis_conn,
            ttl=30,
            hash_string='test4.1'
        )
        self.assertEqual(session2.is_new(), 1)

    def test_lock(self):
        try:
            session = Session(
                redis_conn=self.redis_conn,
                ttl=30,
                hash_string='test3'
            )
            session['name'] = 'Leo'
            session2 = Session(
                sid=session.sid,
                redis_conn=self.redis_conn,
                ttl=30,
                hash_string='test3'
            )
            raise AssertionError("session locking isn't working")
        except SessionError as err:
            pass

    def test_finish(self):
        session = Session(
                redis_conn=self.redis_conn,
                ttl=30,
                hash_string='test5'
            )
        session['name'] = 'Leo'
        session.save(unlock=False)
        session.finish()
        session2 = Session(
                sid=session.sid,
                redis_conn=self.redis_conn,
                ttl=30,
                hash_string='test5'
            )
        self.assertEqual(session.is_new(), 1)
