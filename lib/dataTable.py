#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re
import json
from tornado import gen
import datetime
import logging


class TornadoDataTable ():
    '''
        Simple request handler for JQuery DataTables
            https://www.datatables.net/

        description for HTTP request
        arguments https://www.datatables.net/manual/server-side

        Usage:
            tableHandler = TornadoDataTable(sql_cursor, [column lists],
                                    table, {arguments of HTTP request})
            json_result = yield tableHandler.get_data()

        Example for Tornado requestHandler:
            inner_select = "(SELECT name, surname, age, dept
                            FROM employee ORDER BY surname) t"
            tableHandler = TornadoDataTable(
                                    psql_cur, ['name', 'surname', 'dept'],
                                    inner_select, self.request.arguments)
            json_result = tableHandler.get_data()
            self.finish(json_data)
    '''

    def __init__(self, sql_conn,
                 columns, from_block, arguments):
        self.conn = sql_conn
        self.table = from_block
        self.columns = columns
        self.arg = arguments

    def get_data(self):
        result = self._request()
        if result:
            return self._make_json()

    def _request(self):
        select = '''
            SELECT %s, count(*) OVER() AS full_count
            FROM %s %s %s %s
        ''' % (','.join(self.columns), self.table, self._gen_where(),
               self._gen_order(), self._gen_limit())
        logging.debug(select)
        curr = self.conn.cursor()
        curr.execute(select)
        self.data = curr.fetchall()
        curr.close()
        self.filtered = self.data[0][-1] if len(self.data) else 0
        select = '''
            SELECT count(*)
            FROM %s
        ''' % (self.table)
        logging.debug(select)
        curr = self.conn.cursor()
        curr.execute(select)
        self.total = curr.fetchone()[0]
        curr.close()
        return 1

    def _make_json(self):
        return_obj = {
            'draw': 0,
            'recordsTotal': self.total,
            'recordsFiltered': self.filtered,
            'data': self.data
        }

        # hadler for datetime
        def datetime_json(obj):
            if type(obj) in (datetime, date):
                return obj.strftime('%Y-%m-%d %H:%M:%S')

        try:
            return json.dumps(return_obj, default=datetime_json)
        except TypeError:
            logging.error('_make_json protblem', exc_info=True)
            return

    def _gen_where(self):
        where = ""
        # regexp pattern with bad symbols must be ignored
        re_pattern = re.compile('[%\']')
        searching_string = re_pattern.sub(
                        '',
                        self.arg['search[value]'][0].decode('utf-8'))
        if searching_string and not re_pattern.search(searching_string):
            where = "WHERE "
            for col in self.columns:
                where += "{} LIKE '%%{}%%' OR ".format(col, searching_string)
        return where[:-3]

    def _gen_order(self):
        order = ""
        # the first column in DataTable is 0
        # the first column in PostgreSQL is 1
        column = int(self.arg['order[0][column]'][0]) + 1
        direction = self.arg['order[0][dir]'][0].decode('utf-8')
        if column:
            order = "ORDER BY %s %s" % (column, direction)
        return order

    def _gen_limit(self):
        limit = ""
        start = self.arg['start'][0].decode('utf-8')
        length = self.arg['length'][0].decode('utf-8')
        if start and length:
            limit = "LIMIT %s OFFSET %s" % (length, start)
        return limit
